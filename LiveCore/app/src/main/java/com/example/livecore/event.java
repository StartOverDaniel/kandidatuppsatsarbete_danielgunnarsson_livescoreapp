package com.example.livecore;

public class event {


        public String hemmalag;
        public String bortalag;

        public String tid_1;
        public String tid_2;

        public String event;

        public String hemmalagMål_1;
        public String hemmalagMål_2;

        public String bortalagmål_1;
        public String bortalagmål_2;


    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTid_1() {
        return tid_1;
    }

    public void setTid_1(String tid_1) {
        this.tid_1 = tid_1;
    }

    public String getTid_2() {
        return tid_2;
    }

    public void setTid_2(String tid_2) {
        this.tid_2 = tid_2;
    }

    public String getHemmalag() {
        return hemmalag;
    }

    public void setHemmalag(String hemmalag) {
        this.hemmalag = hemmalag;
    }

    public String getBortalag() {
        return bortalag;
    }

    public void setBortalag(String bortalag) {
        this.bortalag = bortalag;
    }

    public String getHemmalagMål_1() {
        return hemmalagMål_1;
    }

    public void setHemmalagMål_1(String hemmalagMål_1) {
        this.hemmalagMål_1 = hemmalagMål_1;
    }

    public String getHemmalagMål_2() {
        return hemmalagMål_2;
    }

    public void setHemmalagMål_2(String hemmalagMål_2) {
        this.hemmalagMål_2 = hemmalagMål_2;
    }

    public String getBortalagmål_1() {
        return bortalagmål_1;
    }

    public void setBortalagmål_1(String bortalagmål_1) {
        this.bortalagmål_1 = bortalagmål_1;
    }

    public String getBortalagmål_2() {
        return bortalagmål_2;
    }

    public void setBortalagmål_2(String bortalagmål_2) {
        this.bortalagmål_2 = bortalagmål_2;
    }


        public event(String hemmalag, String bortalag, String eventTyp, String hemmamål_1,String hemmamål_2, String bortamål_1, String bortamål_2, String tid_1, String tid_2 ) {

            this.hemmalag = hemmalag;

            this.bortalag = bortalag;

            this.event = eventTyp;

            this.hemmalagMål_1 = hemmamål_1;
            this.hemmalagMål_2 = hemmamål_2;

            this.bortalagmål_1 = bortamål_1;
            this.bortalagmål_2 = bortamål_2;

            this.tid_1 = tid_1;
            this.tid_2 = tid_2;

        }

    }

