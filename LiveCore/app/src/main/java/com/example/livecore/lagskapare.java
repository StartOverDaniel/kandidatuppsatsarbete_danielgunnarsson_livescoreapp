package com.example.livecore;

import android.media.Image;

public class lagskapare{
    public String lagnamn;
    public String arenanamn;
    //public Image logo;
    public String logo;

    public lagskapare(String lagnamn, String arenanamn, String logo){
        this.lagnamn = lagnamn;
        this.arenanamn = arenanamn;
        this.logo = logo;
        }

    public void setLagnamn(String namn) {
        lagnamn = namn;
    }

    public String getLagnamn() {
        return lagnamn;
    }

    public void setArenanamn(String arena) {
        arenanamn = arena;
    }

    public String getArenanamn() {
        return arenanamn;
    }

    public void setImage(String logga){
        logo = logga;

    }

    public String getLogo() {
        return logo;
    }
}