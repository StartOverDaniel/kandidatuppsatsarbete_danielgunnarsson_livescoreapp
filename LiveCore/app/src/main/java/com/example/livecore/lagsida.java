package com.example.livecore;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.logging.Logger;

public class lagsida extends AppCompatActivity {
    ListView lv;
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    TextView lagtext;
    TextView arenatext;
    ImageView logga;
    String value;
    String lag;
    boolean favorit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lagsida);
        Toolbar toolbar = findViewById(R.id.toolbar);
        getExtra();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dl = (DrawerLayout) findViewById(R.id.lag_view);
        t = new ActionBarDrawerToggle(this, dl, R.string.open, R.string.close);

        setupDrawer();
        dl.addDrawerListener(t);
        t.syncState();

        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                                                 @Override
                                                 public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                                                     int id = item.getItemId();
                                                     if (id == R.id.action_hem) {
                                                         //Toast.makeText(MainActivity.this,"Hem",Toast.LENGTH_SHORT).show();
                                                         Intent playIntent = new Intent(lagsida.this, MainActivity.class);
                                                         lagsida.this.startActivity(playIntent);
                                                         return true;
                                                     }
                                                     if (id == R.id.action_matcher) {
                                                         Intent playIntent = new Intent(lagsida.this, matcher.class);
                                                         lagsida.this.startActivity(playIntent);
                                                         // Toast.makeText(MainActivity.this,"Matcher",Toast.LENGTH_SHORT).show();
                                                         return true;
                                                     }
                                                     if (id == R.id.action_lag) {
                                                         Intent playIntent = new Intent(lagsida.this, lag.class);
                                                         lagsida.this.startActivity(playIntent);
                                                         return true;
                                                     }
                                                     if (id == R.id.action_favoriter) {
                                                         Intent playIntent = new Intent(lagsida.this, favoriter.class);
                                                         lagsida.this.startActivity(playIntent);
                                                         return true;
                                                     }
                                                     if (id == R.id.action_profil) {
                                                         Intent playIntent = new Intent(lagsida.this, minprofil.class);
                                                         lagsida.this.startActivity(playIntent);
                                                         return true;
                                                     }
                                                     return true;
                                                 }
                                             });

//        setSupportActionBar(toolbar);
            //sätter lyssnare på alla knappar
            Button kommande = (Button)findViewById(R.id.kommandeBtn);
         //   favoritBtn = (Button)findViewById(R.id.favoritBtn);
            lag = value;
                    kommande.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            kommandeClicked(lag);
                        }
                    });
        }
    public void kommandeClicked(String namn){
        Intent playIntent = new Intent(lagsida.this, matcher.class);
        playIntent.putExtra("hemmalag", "kommande");
        lagsida.this.startActivity(playIntent);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorit, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_favoritBtn) {
            Toast.makeText(lagsida.this,lag+ " Tilllagd i mina favoriter",Toast.LENGTH_SHORT).show();
            return true;
        }*/
        if (id == R.id.action_favoritBtn) {
            if (!favorit) {
                Toast.makeText(lagsida.this, "Tillagd i mina favoriter", Toast.LENGTH_LONG).show();
                item.setIcon(R.drawable.ic_favorite_black_24dp);
                favorit = true;
            } else {
                Toast.makeText(lagsida.this,"Borttagen från mina favoriter",Toast.LENGTH_LONG).show();
                item.setIcon(R.drawable.ic_favorite_border_black_24dp);
                favorit=false;
            }
        }

        return super.onOptionsItemSelected(item);
    }
    public void setupDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.lag_view);
        t = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

    }
    public void getExtra() {
       // Button favorit = (Button)findViewById(R.id.favoritBtn);
        // favorit.setOnClickListener(this);
        //Skaffar refes till alla textfält
        lagtext = (TextView)findViewById(R.id.idTag);
        arenatext = (TextView)findViewById(R.id.arenaTag);
        logga = (ImageView)findViewById(R.id.Bild2);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("Lagnamn");
          //  setTitle("Lag 1");
          /*  lagtext.setText("Lag: Lag 1");
            arenatext.setText("Arena: Arena lag 1");
            logga.setImageResource(R.drawable.soccer);*/
            //The key argument here must match that used in the other activity
            /*Toast toast = Toast.makeText(getApplicationContext(),
                    value, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, -220);
            toast.show();*/

            if (value.equals("Åtorps IF")) {
                lagtext.setText("Lag: Åtorps IF");
                setTitle("Åtorps IF");
                arenatext.setText("Arena: Åvallen");
                logga.setImageResource(R.drawable.aif);
            }
            if (value.equals("Karlskoga IF")) {
                lagtext.setText("Lag: Karlskoga IF");
                setTitle("Karlskoga IF");
                arenatext.setText("Arena: Baggängens IP");
                logga.setImageResource(R.drawable.kif);
            }
            if (value.equals("Strömtorps IK B")) {
                lagtext.setText("Lag: Strömtorps IK B");
                setTitle("Strömtorps IK");
                arenatext.setText("Arena: Strömtorps IP");
                logga.setImageResource(R.drawable.sik);
            }
            if (value.equals("Immetorp BK 2")) {
                lagtext.setText("Lag: Immetorps BK 2");
                setTitle("Immetorps BK 2");
                arenatext.setText("Arena: Källmossen");
                logga.setImageResource(R.drawable.ibk);
            }
        }
    }

}
