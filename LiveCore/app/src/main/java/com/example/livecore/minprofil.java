package com.example.livecore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

public class minprofil extends AppCompatActivity {
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minprofil);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Min profil");
        final Switch simpleSwitch = (Switch) findViewById(R.id.simpleSwitch);
        simpleSwitch.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                if (simpleSwitch.isChecked())
                                                Toast.makeText(minprofil.this,"Notiser på!",Toast.LENGTH_SHORT).show();
                                                else{
                                                    Toast.makeText(minprofil.this,"Notiser av!",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
            //statusSwitch = simpleSwitch.getTextOn().toString();
        dl = (DrawerLayout) findViewById(R.id.profil_view);
        t = new ActionBarDrawerToggle(minprofil.this, dl, R.string.open,R.string.close);

        setupDrawer();
        dl.addDrawerListener(t);
        t.syncState();

        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_hem) {
                    //Toast.makeText(MainActivity.this,"Hem",Toast.LENGTH_SHORT).show();
                    Intent playIntent = new Intent(minprofil.this, MainActivity.class);
                    minprofil.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_matcher) {
                    Intent playIntent = new Intent(minprofil.this, matcher.class);
                    minprofil.this.startActivity(playIntent);
                    // Toast.makeText(MainActivity.this,"Matcher",Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (id == R.id.action_lag) {
                    Intent playIntent = new Intent(minprofil.this, lag.class);
                    Log.i("Intentet:", playIntent.toString());
                    minprofil.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_favoriter) {
                   // Toast.makeText(minprofil.this,"Mina favoriter",Toast.LENGTH_SHORT).show();
                    Intent playIntent = new Intent(minprofil.this, favoriter.class);
                    Log.i("Intentet:", playIntent.toString());
                    minprofil.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_profil) {
                    //Toast.makeText(minprofil.this, "Min profil", Toast.LENGTH_SHORT).show();
                    return true;
                }
                return true;
            }


        });
    }
    public void setupDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.profil_view);
        t = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
    }

}
