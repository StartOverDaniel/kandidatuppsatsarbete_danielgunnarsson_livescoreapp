package com.example.livecore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class customadapter extends ArrayAdapter<event> {

    public customadapter(Context context, ArrayList<event> users) {

        super(context, 0, users);

    }



    @Override

    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position

        event user = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view

        if (convertView == null) {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_adapter, parent, false);

        }

        // Lookup view for data population

        TextView hemmalag = (TextView) convertView.findViewById(R.id.lag_mål);

        TextView bortalag = (TextView) convertView.findViewById(R.id.lag_mål2);

        TextView hemmamål_1 = (TextView) convertView.findViewById(R.id.hemmalag1_mål);

        TextView hemmamål_2 = (TextView) convertView.findViewById(R.id.hemmalag2_mål);

        TextView bortamål_1 = (TextView) convertView.findViewById(R.id.bortalag1_mål);

        TextView bortamål_2 = (TextView) convertView.findViewById(R.id.bortalag2_mål);

        ImageView iv = (ImageView) convertView.findViewById(R.id.eventImage1);

        ImageView iv2 = (ImageView) convertView.findViewById(R.id.eventImage2);

        TextView counter1 = (TextView) convertView.findViewById(R.id.counter1);

        TextView counter2 = (TextView) convertView.findViewById(R.id.counter2);

        LinearLayout lv = (LinearLayout) convertView.findViewById(R.id.hemmalagEvent);

        LinearLayout lv2 = (LinearLayout) convertView.findViewById(R.id.bortalagEvent);

        TextView binde1 = (TextView) convertView.findViewById(R.id.binde1);

        TextView binde2 = (TextView) convertView.findViewById(R.id.binde2);



        // Populate the data into the template view using the data object

        hemmalag.setText(user.hemmalag);

        hemmamål_1.setText(user.hemmalagMål_1);

        hemmamål_2.setText(user.hemmalagMål_2);

        bortalag.setText(user.bortalag);

        bortamål_1.setText(user.bortalagmål_1);

        bortamål_2.setText(user.bortalagmål_2);

        counter1.setText(user.tid_1);

        counter2.setText(user.tid_2);

        if(user.getEvent().equals("mål hemmalag")){
            iv.setImageResource(R.drawable.soccer);
            lv2.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
        }
        if(user.getEvent().equals("mål bortalag")){
            iv2.setImageResource(R.drawable.soccer);
            lv.setVisibility(View.GONE);
            lv2.setVisibility(View.VISIBLE);
        }
        if(user.getEvent().equals("gult kort hemmalag")){
            iv.setImageResource(R.drawable.ic_stop_black_24dp);
            lv2.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            binde1.setVisibility(View.GONE);
        }
        if(user.getEvent().equals("rött kort hemmalag")){
            iv.setImageResource(R.drawable.ic_stop_red_24dp);
            lv2.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            binde1.setVisibility(View.GONE);
        }
        if(user.getEvent().equals("gult kort bortalag")){
            iv2.setImageResource(R.drawable.ic_stop_black_24dp);
            lv.setVisibility(View.GONE);
            lv2.setVisibility(View.VISIBLE);
            binde2.setVisibility(View.GONE);
        }
        if(user.getEvent().equals("rött kort bortalag")){
            iv2.setImageResource(R.drawable.ic_stop_red_24dp);
            lv.setVisibility(View.GONE);
            lv2.setVisibility(View.VISIBLE);
            binde2.setVisibility(View.GONE);
        }

        // Return the completed view to render on screen

        return convertView;

    }

}