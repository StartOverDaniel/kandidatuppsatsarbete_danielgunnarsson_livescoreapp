package com.example.livecore;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class lag extends AppCompatActivity {
    ListView lv;
    ArrayAdapter adapter;
    SearchView searchView;
    TextView tw;
    Spinner spnLocale;
    String sView = "gone";
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private AlertDialog rulesAlert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lag);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dl = (DrawerLayout) findViewById(R.id.lag_view);
        setTitle("Hitta lag");

        t = new ActionBarDrawerToggle(this, dl, R.string.open, R.string.close);

        setupDrawer();
        dl.addDrawerListener(t);
        t.syncState();
        searchView =(SearchView) findViewById(R.id.seachView);
        searchView.setVisibility(View.GONE);
        tw = (TextView) findViewById(R.id.infoText);
        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_hem) {
                    //Toast.makeText(MainActivity.this,"Hem",Toast.LENGTH_SHORT).show();
                    Intent playIntent = new Intent(lag.this, MainActivity.class);
                    lag.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_matcher) {
                    Intent playIntent = new Intent(lag.this, matcher.class);
                    lag.this.startActivity(playIntent);
                    // Toast.makeText(MainActivity.this,"Matcher",Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (id == R.id.action_lag) {
                   /* Intent playIntent = new Intent(lag.this, lag.class);
                    Log.i("Intentet:", playIntent.toString());
                    lag.this.startActivity(playIntent);*/
                    return true;
                }
                if (id == R.id.action_favoriter) {
                    Intent playIntent = new Intent(lag.this, favoriter.class);
                    lag.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_profil) {
                    Intent playIntent = new Intent(lag.this, minprofil.class);
                    lag.this.startActivity(playIntent);
                    return true;
                }
                return true;
            }


        });
        spnLocale = (Spinner)findViewById(R.id.spinner1);

        spnLocale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ListView lw = (ListView) findViewById(R.id.list_view);
                TextView tw = (TextView) findViewById(R.id.infoText);

                switch (position) {
                    case 0:
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        Toast.makeText(parent.getContext(), "Division 6 östra!", Toast.LENGTH_SHORT).show();
                        lw.setVisibility(View.VISIBLE);
                        tw.setVisibility(View.GONE);
                       /* menyval = 1;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/

                        break;
                    case 2:
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                       /* menyval = 2;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/
                        break;
                    case 3:
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                       /* menyval = 3;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/
                        break;
                    case 4:
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        /*menyval = 4;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/
                        break;
                    case 5:
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                        break;

                }
            }
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        // Initializing a new String Array
        String[] teams = new String[]{
                "Åtorps IF",
                "Immetorp BK 2",
                "Karlskoga IF",
                "Strömtorps IK B"


        };


        // Create a List from String Array elements
        final List<String> teams_list = new ArrayList<String>(Arrays.asList(teams));

        // Create an ArrayAdapter from List
        adapter= new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, teams_list);
        lv = (ListView) findViewById(R.id.list_view);

        // DataBind ListView with items from ArrayAdapter
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (teams_list.get(i).equals("Åtorps IF")) {
                    Intent playIntent = new Intent(lag.this, lagsida.class);
                    playIntent.putExtra("Lagnamn", "Åtorps IF");
                    lag.this.startActivity(playIntent);

                }
                if (teams_list.get(i).equals("Immetorp BK 2")) {
                    Intent playIntent = new Intent(lag.this, lagsida.class);
                    playIntent.putExtra("Lagnamn", "Immetorp BK 2");
                    lag.this.startActivity(playIntent);
                }
                if (teams_list.get(i).equals("Karlskoga IF")) {
                    Intent playIntent = new Intent(lag.this, lagsida.class);
                    playIntent.putExtra("Lagnamn", "Karlskoga IF");
                    lag.this.startActivity(playIntent);
                }
                if (teams_list.get(i).equals("Strömtorps IK B")) {
                    Intent playIntent = new Intent(lag.this, lagsida.class);
                    playIntent.putExtra("Lagnamn", "Strömtorps IK B");
                    lag.this.startActivity(playIntent);
                }
            }

        });
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_lag_sök) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    lv.setVisibility(View.VISIBLE);
                    return false;
                }

        });
            if (sView.equals("gone")){
            searchView.setVisibility(View.VISIBLE);
                //lv.setVisibility(View.VISIBLE);
                tw.setVisibility(View.GONE);
                spnLocale.setVisibility(View.GONE);
                sView="visible";}
            else{
                searchView.setVisibility(View.GONE);
                lv.setVisibility(View.GONE);
                tw.setVisibility(View.VISIBLE);
                spnLocale.setVisibility(View.VISIBLE);
                sView="gone";
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

            public void setupDrawer() {
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.lag_view);
                t = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
                    public void onDrawerClosed(View view) {
                        super.onDrawerClosed(view);
                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }

                    public void onDrawerOpened(View drawerView) {
                        super.onDrawerOpened(drawerView);
                        invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                    }
                };

            }
        }

