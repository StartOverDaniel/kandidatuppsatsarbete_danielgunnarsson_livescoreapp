package com.example.livecore;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;



public class matcher extends AppCompatActivity {
    ArrayAdapter aa;
   // public String kommande = "kommande";
    public String hemma="";
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private String valtEvent;
    private String selectedhemmalag;
    private String selectedbortalag;
    private String selectedarena;
    private String selectedDatum;
    ListView lw;
    TextView tw;
    public String[] hemmalag = {"Åtorps IF", "Strömtorps IK B", "Immetorp BK 2", "Karlskoga IF", "Strömtorps IK B", "Karlskoga IF",
            "Karlskoga IF", "Immetorp BK 2", "Åtorps IF", "Strömtorps IK B", "Åtorps IF", "Immetorp BK 2"
    };
 /* public String[] hemmalag = {"Lag 1", "Lag 2", "Lag 3", "Lag 4", "Lag 2", "Lag 4",
          "Lag 4", "Lag 3", "Lag 1", "Lag 2", "Lag 1", "Lag 3"};*/
    private String[] bortalag = {"Karlskoga IF", "Immetorp BK 2", "Åtorps IF", "Strömtorps IK B", "Åtorps IF", "Immetorp BK 2",
            "Åtorps IF", "Strömtorps IK B", "Immetorp BK 2", "Karlskoga IF", "Strömtorps IK B", "Karlskoga IF"};
    private int menyval = 0;
   /* private String[] arenor = {"Åvallen", "Strömtorps IP", "Källmossen", "Baggängens IP", "Strömtorps IP", "Baggängens IP",
            "Baggängens IP", "Källmossen", "Åvallen", "Strömtorps IP", "Åvallen", "Källmossen"};*/
 /*  private String[] bortalag = {"Lag 4", "Lag 3", "Lag 1", "Lag 2", "Lag 1", "Lag 3",
           "Lag 1", "Lag 2", "Lag 3", "Lag 4", "Lag 2", "Lag 4"};*/
    private String[] arenor = {"Arena Lag 1", "Arena Lag 2", "Arena Lag 3", "Arena Lag 4", "Arena Lag 2", "Arena Lag 4",
            "Arena Lag 4", "Arena Lag 3", "Arena Lag 1", "Arena Lag 2", "Arena Lag 1", "Arena Lag 3"};
    private String[] speldatum = {"8-06-2019","8-06-2019","15-06-2019","15-06-2019","22-06-19","22-06-19","29-06-2019","29-06-2019","6-07-2019",
            "6-07-2019","13-07-2019","13-07-2019"};
    private String[] kommandeHemma = {"Åtorps IF", "Immetorp BK 2", "Strömtorps IK B", "Karlskoga IF", "Åtorps IF", "Åtorps IF"};
    private String[] kommandeBorta = {"Karlskoga IF", "Åtorps IF", "Åtorps IF", "Åtorps IF", "Immetorp BK 2", "Strömtorps IK B"};
    private String[] kommandeArenor = {"Åvallen", "Källmossen", "Strömtorps IP", "Baggängens IP", "Åvallen", "Åvallen"};
    private String[] kommandeDatum = {"08-06-2019", "15-06-2019", "22-06-19", "29-06-2019", "06-07-2019", "13-07-2019"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matcher);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lw = (ListView) findViewById(R.id.listVy1);
        tw = (TextView) findViewById(R.id.infoText_matcher);
        setTitle("Spelprogram");
        setContent();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dl = (DrawerLayout) findViewById(R.id.lag_view);
        t = new ActionBarDrawerToggle(this, dl, R.string.open, R.string.close);

        setupDrawer();
        dl.addDrawerListener(t);
        t.syncState();
        Spinner spnLocale = (Spinner)findViewById(R.id.spinner1);

        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_hem) {
                    Intent playIntent = new Intent(matcher.this, MainActivity.class);
                    matcher.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_matcher) {
                   /* Intent playIntent = new Intent(matcher.this, matcher.class);
                    matcher.this.startActivity(playIntent);*/
                    return true;
                }
                if (id == R.id.action_lag) {
                    Intent playIntent = new Intent(matcher.this, lag.class);
                    matcher.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_favoriter) {
                    Intent playIntent = new Intent(matcher.this, favoriter.class);
                    matcher.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_profil) {
                    Intent playIntent = new Intent(matcher.this, minprofil.class);
                    matcher.this.startActivity(playIntent);
                    return true;
                }
                return true;
            }
        });

        final Context context = getApplicationContext();
        Resources resources = getResources();
        TypedArray lag_loggor = resources.obtainTypedArray(R.array.lag_loggor);
        TypedArray lag_loggor2 = resources.obtainTypedArray(R.array.lag_loggor2);
        TypedArray lag_kloggor = resources.obtainTypedArray(R.array.lag_kloggor);
        TypedArray lag_kloggor2 = resources.obtainTypedArray(R.array.lag_kloggor2);


        if(hemma.equals("kommande")) {
            spnLocale.setSelection(1);
            spnLocale.setVisibility(View.GONE);
            tw.setVisibility(View.GONE);

            menyval = 10;
            aa = new matchskapare(context, R.layout.secondary_layout, kommandeHemma, kommandeBorta, lag_kloggor, lag_kloggor2, menyval, kommandeArenor, kommandeDatum);
            lw.setAdapter(aa);}
        else{
            aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2, menyval, arenor, speldatum);
            lw.setAdapter(aa);
        }
        lw.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                selectedhemmalag = (String) parent.getItemAtPosition(position);
                selectedbortalag = bortalag[position];
                selectedarena = arenor[position];
                selectedDatum = speldatum[position];
                AlertDialog.Builder adb = new AlertDialog.Builder(matcher.this);
                adb.setTitle("Vad vill du göra?");
                adb.setMessage("Vill du följa eller liverapportera matchen?");
                adb.setNeutralButton("Följa", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        valtEvent = "följa";
                        matchVy(selectedhemmalag,selectedbortalag,selectedarena,selectedDatum);
                    }
                });
                adb.setPositiveButton("Liverapportera", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        valtEvent = "liverapportera";
                        matchVy(selectedhemmalag,selectedbortalag,selectedarena,selectedDatum);
                    }
                });
                adb.show();
               // matchVy(selectedhemmalag,selectedbortalag,selectedarena,selectedDatum);

            }
        });





        spnLocale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // ListView lw = (ListView) findViewById(R.id.listVy1);

                final Context context = getApplicationContext();
                Resources resources = getResources();
                TypedArray lag_loggor = resources.obtainTypedArray(R.array.lag_loggor);
                TypedArray lag_loggor2 = resources.obtainTypedArray(R.array.lag_loggor2);
                switch (position) {
                    case 0:
                       // Toast.makeText(parent.getContext(), "Division 6 östra!", Toast.LENGTH_SHORT).show();
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        Toast.makeText(parent.getContext(), "Division 6 östra", Toast.LENGTH_SHORT).show();
                        menyval = 10;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);
                        lw.setVisibility(View.VISIBLE);
                        tw.setVisibility(View.GONE);
                        break;
                    case 2:
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                       /* menyval = 2;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                       /* menyval = 3;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                        /*menyval = 4;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        Toast.makeText(parent.getContext(), "Ingen information", Toast.LENGTH_SHORT).show();
                        /*menyval = 4;
                        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2,menyval,arenor,speldatum);
                        lw.setAdapter(aa);*/
                        lw.setVisibility(View.INVISIBLE);
                        tw.setVisibility(View.VISIBLE);
                        break;

                }
            }
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
    }
    public void matchVy(String string_hemma,String string_borta,String arena,String datum){
        Intent playIntent = new Intent(this, matchView.class);
        playIntent.putExtra("hemmalag", string_hemma);
        playIntent.putExtra("bortalag", string_borta);
        playIntent.putExtra("arena", arena);
        playIntent.putExtra("datum", datum);
        playIntent.putExtra("event", valtEvent);
        this.startActivity(playIntent);

    }
    public void setContent() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            hemma = extras.getString("hemmalag");
        }
    }
    public void setupDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.lag_view);
        t = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

    }

}
