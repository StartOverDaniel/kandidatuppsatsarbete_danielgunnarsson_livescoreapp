package com.example.livecore;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class matchskapare extends ArrayAdapter<String> {

    private LayoutInflater inflater;
    private String[] mString;
    private String[] mString2;
    private String[] mArenor;
    private String[] mSpelDatum;
    private TypedArray mLoggor;
    private TypedArray mLoggor2;


    private int mViewResourceId;
    private int mVal;
    public matchskapare(Context ctx, int ViewResourceId, String[] strings,String[] strings2, TypedArray loggor, TypedArray loggor2, int val, String[] arenor,
                        String[] speldatum){

        super(ctx, ViewResourceId, strings);
        inflater =(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mString = strings;
        mString2 = strings2;
        mLoggor = loggor;
        mLoggor2 = loggor2;
        mVal = val;
        mArenor = arenor;
        mSpelDatum = speldatum;

        mViewResourceId = ViewResourceId;

    }
    @Override
    public int getCount(){
        if(mVal == 1){
            return mString.length-10;
        }
        if(mVal == 2){
            return mString.length-8;
        }
        if(mVal == 3){
            return mString.length-6;
        }
        if(mVal == 4){
            return mString.length-4;
        }
        return mString.length;
        }
    @Override
    public String getItem(int position){return mString[position];}
    @Override
    public long getItemId(int position){return 0;}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(mViewResourceId, null);
        //int i = 0;
        ImageView iv = (ImageView) convertView.findViewById(R.id.imagVy1);
        iv.setImageDrawable(mLoggor.getDrawable(position));


        ImageView iv2 = (ImageView) convertView.findViewById(R.id.imagVy2);
        iv2.setImageDrawable(mLoggor2.getDrawable(position));


        TextView tw = (TextView) convertView.findViewById(R.id.textvy1);
        tw.setText(mString[position]);

        TextView tw2 = (TextView) convertView.findViewById(R.id.textvy2);
        tw2.setText(mString2[position]);

        TextView tw3 = (TextView) convertView.findViewById(R.id.arenaid);
        tw3.setText("Spelplats: "+mArenor[position]);

        TextView tw4 = (TextView) convertView.findViewById(R.id.avsparkId);
        tw4.setText(mSpelDatum[position]+"  Avspark: 16:00");



        return convertView;
    }

}
