package com.example.livecore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class favoriter extends AppCompatActivity {
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoriter);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Mina favoriter");

        dl = (DrawerLayout) findViewById(R.id.favoriter_view);
        t = new ActionBarDrawerToggle(favoriter.this, dl, R.string.open,R.string.close);

        setupDrawer();
        dl.addDrawerListener(t);
        t.syncState();

        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_hem) {
                    //Toast.makeText(MainActivity.this,"Hem",Toast.LENGTH_SHORT).show();
                    Intent playIntent = new Intent(favoriter.this, MainActivity.class);
                    favoriter.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_matcher) {
                    Intent playIntent = new Intent(favoriter.this, matcher.class);
                    favoriter.this.startActivity(playIntent);
                    // Toast.makeText(MainActivity.this,"Matcher",Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (id == R.id.action_lag) {
                    Intent playIntent = new Intent(favoriter.this, lag.class);
                    Log.i("Intentet:", playIntent.toString());
                    favoriter.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_favoriter) {
                   /* Toast.makeText(favoriter.this,"Mina favoriter",Toast.LENGTH_SHORT).show();
                    Intent playIntent = new Intent(favoriter.this, favoriter.class);
                    Log.i("Intentet:", playIntent.toString());
                    favoriter.this.startActivity(playIntent);*/
                    return true;
                }
                if (id == R.id.action_profil) {
                   // Toast.makeText(favoriter.this, "Min profil", Toast.LENGTH_SHORT).show();
                    Intent playIntent = new Intent(favoriter.this, minprofil.class);
                    favoriter.this.startActivity(playIntent);
                    return true;
                }
                return true;
            }


        });
// Initializing a new String Array
        final String[] teams = new String[]{
                "Åtorps IF",
                "Strömtorps IK B"


        };


        // Create a List from String Array elements
        final List<String> teams_list = new ArrayList<String>(Arrays.asList(teams));

        // Create an ArrayAdapter from List
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, teams_list);
        ListView lv = (ListView) findViewById(R.id.favoList);

        // DataBind ListView with items from ArrayAdapter
        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(favoriter.this, teams_list.get(i), Toast.LENGTH_SHORT).show();

                if (teams_list.get(i).equals("Åtorps IF")) {
                    Intent playIntent = new Intent(favoriter.this, lagsida.class);
                    playIntent.putExtra("Lagnamn", "Åtorps IF");
                    favoriter.this.startActivity(playIntent);

                }
                if (teams_list.get(i).equals("Strömtorps IK B")) {
                    Intent playIntent = new Intent(favoriter.this, lagsida.class);
                    playIntent.putExtra("Lagnamn", "Strömtorps IK B");
                    favoriter.this.startActivity(playIntent);

                }
            }

                                  });
        final ImageView img1 = (ImageView) findViewById(R.id.cross1);
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder adb = new AlertDialog.Builder(favoriter.this);
                adb.setTitle("Ta bort?");
                adb.setMessage("Vill du ta bort Åtorps IF från dina favoriter? ");
                final int size = teams_list.size();
                adb.setNegativeButton("Nej", null);
                adb.setPositiveButton("Ja", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        img1.setVisibility(View.GONE);
                        teams_list.remove(size - 2);
                        arrayAdapter.notifyDataSetChanged();
                        ImageView logga1 = (ImageView) findViewById(R.id.logga1);
                        logga1.setVisibility(View.GONE);
                    }
                });
                adb.show();
            }
        });
        final ImageView img2 = (ImageView) findViewById(R.id.cross2);
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder adb = new AlertDialog.Builder(favoriter.this);
                adb.setTitle("Ta bort?");
                adb.setMessage("Vill du ta bort Strömtorps IK B från dina favoriter? ");
                final int size = teams_list.size();
                adb.setNegativeButton("Nej", null);
                adb.setPositiveButton("Ja", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        img2.setVisibility(View.GONE);
                        teams_list.remove(size - 1);
                        arrayAdapter.notifyDataSetChanged();
                        ImageView logga2 = (ImageView) findViewById(R.id.logga2);
                        logga2.setVisibility(View.GONE);
                    }
                });
                adb.show();
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favo_options, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search_item) {
            Toast.makeText(favoriter.this," Tilllagd i mina favoriter",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void setupDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.favoriter_view);
        t = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
    }
}
