package com.example.livecore;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    ArrayAdapter aa;
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    private String valtEvent;
    private String selectedhemmalag;
    private String selectedbortalag;
    private String selectedarena;
    private String selectedDatum;
    public String[] hemmalag = {"Åtorps IF", "Strömtorps IK B", "Immetorp BK 2", "Karlskoga IF", "Strömtorps IK B", "Karlskoga IF",
            "Karlskoga IF", "Immetorp BK 2", "Åtorps IF", "Strömtorps IK B", "Åtorps IF", "Immetorp BK 2"
    };
   /* public String[] hemmalag = {"Lag 1", "Lag 2", "Lag 3", "Lag 4", "Lag 2", "Lag 4",
            "Lag 4", "Lag 3", "Lag 1", "Lag 2", "Lag 1", "Lag 3"
    };*/
    private String[] bortalag = {"Karlskoga IF", "Immetorp BK 2", "Åtorps IF", "Strömtorps IK B", "Åtorps IF", "Immetorp BK 2",
            "Åtorps IF", "Strömtorps IK B", "Immetorp BK 2", "Karlskoga IF", "Strömtorps IK B", "Karlskoga IF"};
   /* private String[] bortalag = {"Lag 4", "Lag 3", "Lag 1", "Lag 2", "Lag 1", "Lag 3",
            "Lag 1", "Lag 2", "Lag 3", "Lag 4", "Lag 2", "Lag 4"};*/
    private int menyval = 1;
    private String[] arenor = {"Åvallen", "Strömtorps IP", "Källmossen", "Baggängens IP", "Strömtorps IP", "Baggängens IP",
            "Baggängens IP", "Källmossen", "Åvallen", "Strömtorps IP", "Åvallen", "Källmossen"};
  /*  private String[] arenor = {"Arena Lag 1", "Arena Lag 2", "Arena Lag 3", "Arena Lag 4", "Arena Lag 2", "Arena Lag 4",
            "Arena Lag 4", "Arena Lag 3", "Arena Lag 1", "Arena Lag 2", "Arena Lag 1", "Arena Lag 3"};*/
    private String[] speldatum = {"08-06-2019", "08-06-2019", "15-06-2019", "15-06-2019", "22-06-19", "22-06-19", "29-06-2019", "29-06-2019", "06-07-2019",
            "06-07-2019", "13-07-2019", "13-07-2019"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Dagens matcher");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ListView lw = (ListView) findViewById(R.id.list_view_dagens_matcher);

        final Context context = getApplicationContext();
        Resources resources = getResources();
        TypedArray lag_loggor = resources.obtainTypedArray(R.array.lag_loggor);
        TypedArray lag_loggor2 = resources.obtainTypedArray(R.array.lag_loggor2);

        aa = new matchskapare(context, R.layout.secondary_layout, hemmalag, bortalag, lag_loggor, lag_loggor2, menyval, arenor, speldatum);
        lw.setAdapter(aa);

        dl = (DrawerLayout) findViewById(R.id.activity_main);
        t = new ActionBarDrawerToggle(MainActivity.this, dl, R.string.open,R.string.close);

        setupDrawer();
        dl.addDrawerListener(t);
        t.syncState();

        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_hem) {
                    //Toast.makeText(MainActivity.this,"Hem",Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (id == R.id.action_matcher) {
                    Intent playIntent = new Intent(MainActivity.this, matcher.class);
                    MainActivity.this.startActivity(playIntent);
                    // Toast.makeText(MainActivity.this,"Matcher",Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (id == R.id.action_lag) {
                    Intent playIntent = new Intent(MainActivity.this, lag.class);
                    Log.i("Intentet:", playIntent.toString());
                    MainActivity.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_favoriter) {
                    Intent playIntent = new Intent(MainActivity.this, favoriter.class);
                    Log.i("Intentet:", playIntent.toString());
                    MainActivity.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_profil) {
                    Intent playIntent = new Intent(MainActivity.this, minprofil.class);
                    Log.i("Intentet:", playIntent.toString());
                    MainActivity.this.startActivity(playIntent);
                    return true;
                }
                return true;
            }


            });

        lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                selectedhemmalag = (String) parent.getItemAtPosition(position);
                selectedbortalag = bortalag[position];
                selectedarena = arenor[position];
                selectedDatum = speldatum[position];
                AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                adb.setTitle("Vad vill du göra?");
                adb.setMessage("Vill du följa eller liverapportera matchen?");
                adb.setNeutralButton("Följa", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        valtEvent = "följa";
                        matchVy(selectedhemmalag,selectedbortalag,selectedarena,selectedDatum);
                    }
                });
                adb.setPositiveButton("Liverapportera", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        valtEvent = "liverapportera";
                        matchVy(selectedhemmalag,selectedbortalag,selectedarena,selectedDatum);
                    }
                });
                adb.show();
                //matchVy(selectedhemmalag, selectedbortalag, selectedarena, selectedDatum);


            }
        });

    }
    public void matchVy(String string_hemma,String string_borta,String arena,String datum){
        Intent playIntent = new Intent(this, matchView.class);
        playIntent.putExtra("hemmalag", string_hemma);
        playIntent.putExtra("bortalag", string_borta);
        playIntent.putExtra("arena", arena);
        playIntent.putExtra("datum", datum);
        playIntent.putExtra("event", valtEvent);
        this.startActivity(playIntent);

    }

    public void setupDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main);
        t = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
    }


    }