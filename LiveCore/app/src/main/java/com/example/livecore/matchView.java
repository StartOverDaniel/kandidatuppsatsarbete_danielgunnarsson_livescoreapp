package com.example.livecore;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class matchView extends AppCompatActivity {
    TextView textView, hemmalagMål, bortalagMål;
    TextView hemmalagNamn, bortalagNamn, datumText, arenaNamn;
    int hemmamål, bortamål;
    ListView list, list2;
    public int counter;
    Button angrabtn, button1, button2;
    private AlertDialog rulesAlert;
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;
    String mål, test1, test2;
    CountDownTimer matchTimer;
    ArrayAdapter<String> adapter, adapter2;
    ArrayList<String> listItems = new ArrayList<String>();
    ArrayList<String> listItems2 = new ArrayList<String>();
    boolean halvtid = false;
    Context context;
    String[] elename, nos;
    ArrayList<event> arrayOfUsers = new ArrayList<event>();
    String [] eventTyp = new String[3];
    String [] eventLag = new String[3];
    String [] eventTid = new String[3];
    String [] eventHemmamål = new String[3];
    String [] eventBortamål = new String[3];
    ListView listView;
    String lastEvent;
    customadapter adapter21;
    boolean event_exits =false;
    String h;
    Boolean notis =false;
    ArrayAdapter aa;
    int eventCounter = 0;
    String hometeam;
    String awayteam;
    String hometeamscore;
    String awayteamscore;
    String tid;
    int positionToRemove;
    int position2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        list = findViewById(R.id.eventList);
        //  list2 = findViewById(R.id.eventList2);
        hemmalagMål = findViewById(R.id.textTeam1Score);
        bortalagMål = findViewById(R.id.textTeam2Score);
        button1 = (Button) findViewById(R.id.actionBtn);
        angrabtn = (Button) findViewById(R.id.angraBtn);
        button2 = (Button) findViewById(R.id.testaNotis);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNotification();
            }
        });
        button2.setVisibility(View.GONE);
        setContent();
       // Bundle extras = getIntent().getExtras();


        context = getApplicationContext();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

// Create the adapter to convert the array to views
        final customadapter adapter21 = new customadapter(this, arrayOfUsers);

// Attach the adapter to a ListView
        listView = (ListView) findViewById(R.id.eventList);

        listView.setAdapter(adapter21);


        textView = (TextView) findViewById(R.id.timerId);
        angrabtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (event_exits == true) {
                    AlertDialog.Builder adb = new AlertDialog.Builder(matchView.this);
                    final int size = arrayOfUsers.size();
                    // final int positionToRemove = arrayOfUsers.get(arrayOfUsers.size() - 1);
                    adb.setTitle("Ta bort händelse?");
                    adb.setMessage("Ta bort sista tillagda händelsen? " +
                            "Om nej klicka på den händels du vill ta bort");
                    adb.setNeutralButton("Nej", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                                    AlertDialog.Builder adb = new AlertDialog.Builder(matchView.this);
                                    adb.setTitle("Ta bort?");
                                    adb.setMessage("Vill du ta bort rad " + position);
                                    positionToRemove = position;
                                    adb.setNegativeButton("Nej", null);
                                    adb.setPositiveButton("Ja", new AlertDialog.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            arrayOfUsers.remove(positionToRemove);
                                            adapter21.notifyDataSetChanged();
                                            Toast.makeText(matchView.this, "Tog bort händelse på rad "+positionToRemove, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    adb.show();
                                }
                            });
                        }
                    });

                    adb.setPositiveButton("Ja", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (lastEvent.equals("hemma")) {
                                hemmamål--;
                                mål = Integer.toString(hemmamål);

                                hemmalagMål.setText(mål);
                               // Toast.makeText(matchView.this, "hemma", Toast.LENGTH_SHORT).show();
                            }
                            if (lastEvent.contains("borta")) {
                                bortamål--;
                                mål = Integer.toString(bortamål);
                                bortalagMål.setText(mål);
                               // Toast.makeText(matchView.this, "borta", Toast.LENGTH_SHORT).show();
                            }
                            arrayOfUsers.remove(size - 1);
                            adapter21.notifyDataSetChanged();
                            Toast.makeText(matchView.this, "Tog bort senast tillagda händelse", Toast.LENGTH_SHORT).show();
                        }
                    });
                    adb.show();


                }
            }
        });


        dl = (DrawerLayout) findViewById(R.id.match_view);
        t = new ActionBarDrawerToggle(matchView.this, dl, R.string.open, R.string.close);

        setupDrawer();
        dl.addDrawerListener(t);
        t.syncState();

        nv = (NavigationView) findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.action_hem) {
                    //Toast.makeText(MainActivity.this,"Hem",Toast.LENGTH_SHORT).show();
                    Intent playIntent = new Intent(matchView.this, MainActivity.class);
                    matchView.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_matcher) {
                    Intent playIntent = new Intent(matchView.this, matcher.class);
                    matchView.this.startActivity(playIntent);
                    // Toast.makeText(MainActivity.this,"Matcher",Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (id == R.id.action_lag) {
                    Intent playIntent = new Intent(matchView.this, lag.class);
                    matchView.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_favoriter) {
                    Intent playIntent = new Intent(matchView.this, favoriter.class);
                    matchView.this.startActivity(playIntent);
                    return true;
                }
                if (id == R.id.action_profil) {
                    Intent playIntent = new Intent(matchView.this, minprofil.class);
                    matchView.this.startActivity(playIntent);
                    return true;
                }
                return true;
            }
        });

        textView = (TextView) findViewById(R.id.timerId);
        button1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(matchView.this);
                builder.setCancelable(false);//tvingar användaren att svara på dialogen.
                builder.setTitle("Ny Händelse - Vad har hänt?");
                builder.setItems(new CharSequence[]
                                {"Matchstart", "Halvtid", "Slutsignal", "Mål hemmalag", "Mål bortalag", "Gult kort hemmalag", "Gult kort bortalag",
                                        "Rött kort hemmalag", "Rött kort bortalag"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                switch (id) {
                                    case 0:

                                        if (halvtid == false) {
                                         //   Toast.makeText(matchView.this, "Start", Toast.LENGTH_SHORT).show();
                                            matchTimer = new CountDownTimer(2700000, 60000) {
                                                public void onTick(long millisUntilFinished) {
                                                    textView.setText(String.valueOf(counter + "´"));
                                                    counter++;
                                                    hemmamål = 0;
                                                    bortamål = 0;
                                                }

                                                public void onFinish() {
                                                    textView.setText("45+");
                                                }
                                            }.start();
                                        } else {
                                         //   Toast.makeText(matchView.this, "Start", Toast.LENGTH_SHORT).show();
                                            new CountDownTimer(2700000, 60000) {
                                                public void onTick(long millisUntilFinished) {
                                                    textView.setText(String.valueOf(counter + "´"));
                                                    counter++;
                                                    //hemmamål=0;
                                                    //bortamål=0;
                                                }

                                                public void onFinish() {
                                                    textView.setText("90+");
                                                }
                                            }.start();
                                        }

                                        break;
                                    case 1:
                                        Toast.makeText(matchView.this, "Halvtid", Toast.LENGTH_SHORT).show();
                                        if (matchTimer != null) {
                                            matchTimer.cancel();
                                            matchTimer = null;
                                        }
                                        textView.setText("Halvtid");
                                        counter = 45;
                                        halvtid = true;
                                        break;
                                    case 2:
                                        Toast.makeText(matchView.this, "Slut", Toast.LENGTH_SHORT).show();
                                        if (matchTimer != null) {
                                            matchTimer.cancel();
                                            matchTimer = null;
                                        }
                                        textView.setText("Slut");
                                        break;
                                    case 3:
                                        lastEvent="hemma";

                                       // Toast.makeText(matchView.this, "Mål hemmalag", Toast.LENGTH_SHORT).show();
                                      //  Toast.makeText(matchView.this, lastEvent, Toast.LENGTH_SHORT).show();
                                        hemmamål++;
                                        mål = Integer.toString(hemmamål);
                                        hemmalagMål.setText(mål);
                                        hometeam = hemmalagNamn.getText().toString();
                                        hometeamscore = hemmalagMål.getText().toString();
                                        awayteamscore = bortalagMål.getText().toString();
                                        tid = Integer.toString(counter);

                                        event newUser = new event( hometeam,"", "mål hemmalag", hometeamscore, "", awayteamscore, "", tid+"`", "");
                                        adapter21.add(newUser);
                                        event_exits =true;
                                        break;
                                    case 4:
                                        //Toast.makeText(matchView.this, "mål bortalag", Toast.LENGTH_SHORT).show();
                                        bortamål++;
                                        mål = Integer.toString(bortamål);
                                        bortalagMål.setText(mål);
                                        lastEvent="borta";
                                        awayteam = bortalagNamn.getText().toString();
                                        hometeam = hemmalagNamn.getText().toString();
                                        hometeamscore = hemmalagMål.getText().toString();
                                        awayteamscore = bortalagMål.getText().toString();
                                        tid = Integer.toString(counter);

                                        event newUser2 = new event("",awayteam,"mål bortalag","",hometeamscore,"",awayteamscore,"",tid+"`");
                                        adapter21.add(newUser2);
                                        event_exits =true;
                                        break;
                                    case 5:
                                        lastEvent="gult";
                                        //Toast.makeText(matchView.this, "Gult kort hemmalag", Toast.LENGTH_SHORT).show();
                                        hometeam = hemmalagNamn.getText().toString();
                                        tid = Integer.toString(counter);
                                        event gult_hemma = new event(hometeam,"","gult kort hemmalag","","","","",tid+"´","");
                                        adapter21.add(gult_hemma);
                                        break;
                                    case 6:
                                        lastEvent="gult";
                                      //  Toast.makeText(matchView.this, "gult kort bortalag", Toast.LENGTH_SHORT).show();
                                        awayteam = bortalagNamn.getText().toString();
                                        tid = Integer.toString(counter);
                                        event gult_borta = new event("",awayteam,"gult kort bortalag","","","","","", tid+"´");
                                        adapter21.add(gult_borta);
                                        event_exits =true;
                                        break;
                                    case 7:
                                        lastEvent="rött";
                                   //     Toast.makeText(matchView.this, "Röttkort hemmalag", Toast.LENGTH_SHORT).show();
                                        hometeam = hemmalagNamn.getText().toString();
                                        tid = Integer.toString(counter);
                                        event rött_hemma = new event(hometeam,"","rött kort hemmalag","","","","",tid+"`", "");
                                        adapter21.add(rött_hemma);
                                        event_exits =true;
                                        break;
                                    case 8:
                                        lastEvent="rött";
                                    //    Toast.makeText(matchView.this, "Rött kort bortalag", Toast.LENGTH_SHORT).show();
                                        awayteam = bortalagNamn.getText().toString();
                                        tid = Integer.toString(counter);
                                        event rött_borta = new event("",awayteam,"rött kort bortalag","","","","","", tid+"`");
                                        adapter21.add(rött_borta);
                                        event_exits =true;
                                        break;
                                }
                            }
                        });
                builder.setPositiveButton("Avbryt",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                rulesAlert = builder.create();
                builder.show();
            }

        });
    }
    private void addNotification(){
        //bygg notisen
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.soccer)
                .setContentTitle("Mål")
                .setContentText("27´ Åtorps IF")
                .setDefaults(Notification.DEFAULT_SOUND);

        //skapa ett intent för att få notisen
        Intent notificationIntent = new Intent(this,matchView.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this,0,notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        //add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

        public void setContent() {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String hemma = extras.getString("hemmalag");
                String borta = extras.getString("bortalag");
                String arena = extras.getString("arena");
                String date = extras.getString("datum");
                String event = extras.getString("event");
                h = event;
                //The key argument here must match that used in the other activity
                hemmalagNamn = findViewById(R.id.textViewTeam1);
                bortalagNamn = findViewById(R.id.textViewTeam2);
                ImageView lag1Bild = findViewById(R.id.imageViewTeam1);
                lag1Bild.setImageResource(R.drawable.soccer);
                ImageView lag2Bild = findViewById(R.id.imageViewTeam2);
                lag2Bild.setImageResource(R.drawable.soccer);
                hemmalagNamn.setText(hemma);
                bortalagNamn.setText(borta);
             /*   hemmalagNamn.setText("Lag1");
                bortalagNamn.setText("Lag4");*/
                setTitle(hemma + " - " + borta);
                //setTitle("Lag1" + " - " + "Lag4");
                textView = (TextView) findViewById(R.id.timerId);
                textView.setText("16:00");
                //setTitle(hemma);
                if (hemma.equals("Åtorps IF")) {
                    //lag1Bild = findViewById(R.id.imageViewTeam1);
                    lag1Bild.setImageResource(R.drawable.aif);
                }
                if (hemma.equals("Strömtorps IK B")) {
                    //lag1Bild = findViewById(R.id.imageViewTeam1);
                    lag1Bild.setImageResource(R.drawable.sik);
                }
                if (hemma.equals("Immetorp BK 2")) {
                    //lag1Bild = findViewById(R.id.imageViewTeam1);
                    lag1Bild.setImageResource(R.drawable.ibk);
                }
                if (hemma.equals("Karlskoga IF")) {
                    //lag1Bild = findViewById(R.id.imageViewTeam1);
                    lag1Bild.setImageResource(R.drawable.kif);
                }
                if (borta.equals("Åtorps IF")) {

                    //lag2Bild = findViewById(R.id.imageViewTeam2);
                    lag2Bild.setImageResource(R.drawable.aif);
                }
                if (borta.equals("Strömtorps IK B")) {
                    //lag2Bild = findViewById(R.id.imageViewTeam2);
                    lag2Bild.setImageResource(R.drawable.sik);
                }
                if (borta.equals("Immetorp BK 2")) {
                   // lag2Bild = findViewById(R.id.imageViewTeam2);
                    lag2Bild.setImageResource(R.drawable.ibk);
                }
                if (borta.equals("Karlskoga IF")) {
                   // lag2Bild = findViewById(R.id.imageViewTeam2);
                    lag2Bild.setImageResource(R.drawable.kif);
                }
                if(event.equals("följa")){
                    button1.setVisibility(View.GONE);
                    angrabtn.setVisibility(View.GONE);
                    button2.setVisibility(View.VISIBLE);
                }

            }
        }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(h.equals("följa")){
        getMenuInflater().inflate(R.menu.menu_notis, menu);}
        else {
           // Toast.makeText(matchView.this,"Du rapporterar",Toast.LENGTH_LONG).show();
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_följ_match) {
            if(!notis){
            Toast.makeText(matchView.this,"Du blir nu notifierad vid uppdateringar i matchen",Toast.LENGTH_LONG).show();
            item.setIcon(R.drawable.ic_add_alert_black_24dp);
            notis=true;}
            else {
                Toast.makeText(matchView.this,"Tog bort notiser för matchen",Toast.LENGTH_LONG).show();
                item.setIcon(R.drawable.ic_add_alert_white_24dp);
                notis=false;
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void setupDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.match_view);
        t = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

    }
}
